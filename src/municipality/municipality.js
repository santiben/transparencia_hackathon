'use strict'

const request = require('request')
const Municipality = require('../db/models/municipality')
const config = require('../../config')

module.exports = {
    create: async (req, response) => {
        const { body } = req
        try {
            Municipality.findOne({ municipio: body.municipio }, function (error, doc) {
                if (error) return response.status(500).send(error)
                console.log(doc)
                if (doc) return response.status(500).send(new Error(`Municipality with key ${doc.municipio} elready exist`))

                const params = { 
                    searchtext: body.municipio, 
                    app_id: config.HERE_API.app_id,
                    app_code: config.HERE_API.app_code
                }

                request.get(config.HERE_API.url, { qs: params }, async function (error, res, data) {
                    if (error) return reject(error)
                    
                    const munici = await Municipality.create(body)

                    data =  JSON.parse(data)
                    const location = data.Response.View.length === 0 ? null : data.Response.View[0]
                    munici.Latitude = location.Result[0].Location.DisplayPosition.Latitude
                    munici.Longitude = location.Result[0].Location.DisplayPosition.Longitude
                    await munici.save()

                    response.status(200).send(munici)
                })
            })
        } catch (error) {
            response.status(500).send(error)
        }
    },
    update: async (request, response) => {
        const { params: { key }, body } = request
        
        Municipality.findOne({ _id: key }, async function (error, doc) {
            if (error) return response.status(500).send(error)

            if (!doc) return response.status(500).send(new Error(`Municipality with key ${id} not exist`))

           try {
            Object.assign(doc, body)
            await doc.save()

            response.status(200).send(doc)
           } catch (error) {
               response.status(500).send(error)
           }
        })
    },
    getAll: async (req, response) => {
        const { query } = req

        const pagination = {}
        pagination.page = parseInt(query.page) || 1
        pagination.limit = parseInt(query.limit) || 10
        Municipality.paginate({}, pagination, async function (error, municipalities) {
            if (error) return response.status(500).send(error)

            response.status(200).send(municipalities)
        })
    },
    get: async (request, response) => {
        const { key } = request.params
        Municipality.findOne({ municipio: key }, async function (error, municipality) {
            if (error) return response.status(500).send(error)

            response.status(200).send(municipality)
        })
    }
}