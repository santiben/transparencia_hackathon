'use strict'

const router = require('express').Router()
const municipalityCtrl = require('./municipality')

router.post('', municipalityCtrl.create)
router.put('/:key', municipalityCtrl.update)
router.get('/list', municipalityCtrl.getAll)
router.get('/:key', municipalityCtrl.get)

module.exports = router