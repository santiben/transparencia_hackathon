'use strict'

const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')
const Schema = mongoose.Schema

const alertSchema = new Schema({
    email: String,
    user: String,
    message: String,
    date: { type: Date, default: Date.now() }
})

alertSchema.plugin(mongoosePaginate)

module.exports = mongoose.model('alerts', alertSchema)