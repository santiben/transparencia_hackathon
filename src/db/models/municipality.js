'use strict'

const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')
const Schema = mongoose.Schema

const municipalitySchema = new Schema({
    municipio: { type: String },
    Latitude: String,
    Longitude: String,
    propiedades: Schema.Types.Mixed
})

municipalitySchema.plugin(mongoosePaginate)

module.exports = mongoose.model('municipios', municipalitySchema)