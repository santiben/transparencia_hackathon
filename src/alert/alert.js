'use strict'

const nodemailer = require('../middleware/nodemailer')
const alert = require('../db/models/alert')

module.exports = {
    create: async (request, response) => {
        const { body } = request
        await alert.create(body)
        await nodemailer(body.email, { user: body.user, message: body.message })
        response.status(200).send({ message: 'Enviado' })
    },
    getAll: async (request, response) => {
        const { query } = request

        const pagination = {}
        pagination.page = query.page || 1
        pagination.limit = query.limit || 10
        alert.paginate({}, pagination, function (error, docs) {
            if (error) return response.status(500).send(error)

            response.status(200).send(docs)
        })
    }
}