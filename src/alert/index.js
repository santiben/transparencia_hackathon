'use strict'

const router = require('express').Router()
const alertCtrl = require('./alert')

router.post('', alertCtrl.create)
router.get('', alertCtrl.getAll)

module.exports = router