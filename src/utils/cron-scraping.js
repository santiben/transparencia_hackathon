'use strict'

//const cron = require('node-cron')
const xml2js = require('xml2js');
const fs = require('fs');
const path = require('path')
const parser = new xml2js.Parser({ attrkey: "ATTR" });
const readXlsxFile = require('read-excel-file/node');
const XLSX = require('xlsx')
const Municipality = require('../db/models/municipality')
const csvtojson = require("csvtojson");

const config = require('../../config')

/*cron.schedule('3/1 * * * *', () => {
    console.log('Read documents')
})*/

/*function readDocument () {
    fs.readdir(config.SERVER.private_path, async function (error, files) {
        if (error) return console.log(error)

        let xls = files.map(file => {
            return new Promise((resolve, reject) => {
                const pathname = path.join(config.SERVER.private_path, file)
                const workbook = XLSX.readFile(pathname);
                const sheet_name_list = workbook.SheetNames;
                const xlData = XLSX.utils.sheet_to_csv(workbook.Sheets[sheet_name_list[0]]);
                cvs.fromString(xlData)
                    .on('data', function (data) {
                        console.log(data)
                        resolve(data)
                    })
            })
        })

        xls = await Promise.all(xls)
    })
}*/

function readDocument () {
    fs.readdir(config.SERVER.private_path, async function (error, files) {
        if (error) return console.log(error)

        let xls = files.map(file => {
            return new Promise((resolve, reject) => {
                const pathname = path.join(config.SERVER.private_path, file)
                console.log(pathname)
                csvtojson()
                    .fromFile(pathname)
                    .then(csvData => {
                        console.log(csvData);
                    });
            })
        })
    })
}

readDocument()