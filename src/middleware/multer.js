'use strict'

const multer = require('multer')

const config = require('../../config')
const path = require('path')

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, config.SERVER.private_path)
    },
    filename: function (req, file, cb) {
        const extension = path.extname(file.originalname)
        const pathname = `${file.fieldname}-${Date.now()}${extension}`
        cb(null, pathname)
    }
})
   
const upload = multer({ storage: storage })

module.exports = upload