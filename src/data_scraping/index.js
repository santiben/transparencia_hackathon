'use strict'

const uploads = require('../middleware/multer')
const router = require('express').Router()
const dataScraping = require('../data_scraping/data_scraping')

router.post('/upload', uploads.single('doc'), dataScraping.uploadData)

module.exports = router
