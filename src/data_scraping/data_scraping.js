'use strict'

const debug = require('debug')('trans_api:api:scraping')

module.exports = {
    uploadData: async (request, response) => {
        debug('A request has come to POST method uploadData')
        
        const { file } = request

        response.status(200).end()
    }
}