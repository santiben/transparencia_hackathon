'use strict'

const path = require('path')

module.exports = {
    SERVER: {
        public_path: path.join(__dirname, 'public/uploads'),
        private_path: path.join(__dirname, '/private/uploads')
    },
    HERE_API: {
        url: 'https://geocoder.api.here.com/6.2/geocode.json',
        app_id: '5SzZvNIuX9hhOu1M3VGZ',
        app_code: 'VF7qI6l-p9mcRpUG1VillA'
    },
    MONGODB: 'mongodb://localhost:27017/transparencia',
    PORT: 3000
}