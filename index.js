'use strict'

const express = require('express')
const config = require('./config')
const mongoose = require('mongoose')
const cors = require('cors')
const dataScraping = require('./src/data_scraping')
const municipality = require('./src/municipality')
const alert = require('./src/alert')

const app = express()

app.use(cors())
app.use(express.json())

app.use('/api/scraping', dataScraping)
app.use('/api/municipality', municipality)
app.use('/api/alert', alert)

mongoose.connect(config.MONGODB, { useNewUrlParser: true, useUnifiedTopology: true });

app.listen(config.PORT, () => {
    console.log(`SERVER LISTEN IN PORT ${config.PORT}`)
})